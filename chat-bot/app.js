require('dotenv').config()

const sha256 = require('sha256')
const io = require('socket.io-client')

const Bot = require('./lib/Bot')
const BotHandler = require('./lib/BotHandler')

// Connect the server
let socket = io.connect(process.env.SERVER_URL + ':' + process.env.SERVER_PORT)

// Inject the hash module to the bot
let bot = new Bot( sha256 )

// Inject both the bot and the socket to the handler
let botHandler = new BotHandler( bot, socket )

// Bind all the socket events
socket.on('connect', botHandler.onConnect.bind( botHandler ))
socket.on('disconnect', botHandler.onDisconnect.bind( botHandler ))
socket.on('chat:msg', botHandler.onMessage.bind( botHandler ))