/**
 * This class represents the bot
 * The bot saves the questions in a hash table (although there isn't hash table in JS)
 * Instead of saving the questions "as is", I decided to lowercase it and trim it
 * then hash it with simple sha256
 *
 * The bot checks each message, if it is a question (means is starts with a question word and ends with question mark)
 * it searches for the answer, if it doesn't find the answer it saves the question in the context, waiting
 * for the answer to come
 *
 * When next messages pops it checks the context, if the context is pending it will associate the answer
 * to the question
 */
const sha256 = require('sha256')

class Bot {
    /**
     * Bot Constructor
     * @param {sha256} hasher
     */
    constructor( hasher ) {
        this.dictionary = {}
        this.hasher = hasher
        this.context = null
    }

    /**
     * Clears the dictionary, sets empty object
     */
    resetDictionary() {
        this.dictionary = {}
    }

    /**
     * Checks if the input is in question structure
     * Question structure is when the input starts with one of the 'Wh' questions
     * and ends with a question mark
     * @param input
     * @returns {boolean}
     */
    static isQuestion( input ) {
        return /^(who|what|when|where|why|which|whom|whose|how|does|do|is|are)(.+)\?$/.test( input.toLowerCase() )
    }

    /**
     * Making the string act as a hash
     * e.g. "how is it going today?" will be "howisitgoingtoday"
     * @param input
     * @returns {string}
     */
    static toHash( input ) {
        // Remove the question mark, trim and remove spaces
        return sha256( input.slice(0, -1).toLowerCase().trim() )
    }

    /**
     * Checks if the answer exists in the storage (dictionary)
     * @param input
     * @returns {*}
     */
    answerExists( input ) {
        let question = Bot.toHash( input )

        if ( this.dictionary.hasOwnProperty( question ) )
            return this.dictionary[ question ]

        return false
    }

    /**
     * Hash the answer and insert it to the storage
     * @param question
     * @param answer
     */
    saveAnswer( answer ) {
        this.dictionary[ this.context ] = answer
    }

    /**
     * Clear the context
     */
    clearContext() {
        this.context = null
    }

    /**
     * Set the context to be a new question waiting for answer
     * @param input
     */
    setContext( input ) {
        this.context = Bot.toHash( input )
    }

    /**
     * Is context empty
     * @returns {boolean}
     */
    isContextEmpty() {
        return this.context === null
    }

    /**
     * This method parses the input, checking whether it's a question or not
     * if it is a question it searches for an answer, if it does not find one
     * it saves the question in the context property
     * if it is not a question it checks if the context is pending (means it has something in it)
     * and if it does, it saves the context (as a hash) with the answer in the dictionary
     * @param input
     * @returns {*}
     */
    parseInput( input ) {
        if ( Bot.isQuestion( input ) ) {
            // Check for answer for this question
            let answer = this.answerExists( input )
            if ( answer )
                return answer

            // If answer has not been found in the dictionary, save the question in the context
            this.setContext( input )
            return false
        } else {
            // It's not a question for sure, so it must be an answer
            if ( !this.isContextEmpty() ) {
                // The context is full (holding a question), bind the answer to it and clear the context
                this.saveAnswer( input )
                this.clearContext()
            }

            return false
        }
    }
}

module.exports = Bot