require('dotenv').config()
const Bot = require('./Bot')

class BotHandler {
    /**
     * @param {Bot} bot
     * @param {socket.io-client} socket
     */
    constructor( bot, socket ) {
        this.bot = bot
        this.socket = socket
        this.username = process.env.BOT_NICKNAME // Bot username, see '.env' file
    }

    /**
     * Emits chat join message with the username as the data
     */
    joinChat() {
        this.socket.emit('chat:join', this.username)
    }

    /**
     * Emits chat msg with an object including the message and the bot username
     * @param msg
     */
    sendMessage( msg ) {
        this.socket.emit('chat:msg', { msg , username: this.username })
    }

    /**
     * Emits the join chat message
     * Send welcome message as well
     */
    onConnect() {
        this.joinChat()
        this.sendMessage(`Hey everyone, I am ${this.username} and I will try to help you here with some questions`)
        console.info('Connected to chat')
    }

    /**
     * Disconnect event handling
     */
    onDisconnect() {
        console.log('%s is now disconnected, connection lost', process.env.BOT_NICKNAME)
    }

    /**
     * Checks the context and decides whether to answer or not
     * @param data
     */
    onMessage( data ) {
        if ( Bot.isQuestion( data.msg ) )
            this.sendMessage('Good question!')

        let answer = this.bot.parseInput( data.msg )
        if ( answer )
            this.sendMessage(`I found an answer for you ${data.username}: ${answer}`)
    }
}

module.exports = BotHandler
