const Bot = require('../lib/Bot')
const sha256 = require('sha256')

describe('Bot', () => {
    beforeEach(() => {
        this.bot = new Bot( sha256 )
        this.bot.clearContext()
        this.bot.resetDictionary()
    })

    it('Should identify question regex', () => {
        expect( Bot.isQuestion('Why the sky is blue?') ).toBeTruthy()
        expect( Bot.isQuestion('Do cows drink milk?') ).toBeTruthy()
        expect( Bot.isQuestion('Why the sky is blue') ).toBeFalsy()
        expect( Bot.isQuestion('the sky is blue?') ).toBeFalsy()
    })

    it('Hashing', () => {
        expect( Bot.toHash('some-test-string').length ).toEqual(64)
    })

    it('Should hash the question and put it in the context', () => {
        let question = 'Why is the sky blue?'
        this.bot.setContext( question )
        expect( this.bot.context.length ).toEqual(64)
    })

    it('Should insert answer to the dictionary', () => {
        let question = 'Why is the sky blue?'
        let answer = 'Because the sun makes it clearer'
        this.bot.setContext( question )
        this.bot.saveAnswer( answer )
        this.bot.clearContext()

        let hash = Bot.toHash( question )
        expect( this.bot.dictionary.hasOwnProperty( hash ) ).toBeTruthy()
        expect( this.bot.dictionary[ hash ] ).toEqual( answer )
    })

    it('Should save the question in the context and find an answer', () => {
        let question = 'What is the weather today?'
        let answer = 'It is pretty sunny out there'
        let found = this.bot.parseInput( question )

        expect( this.bot.isContextEmpty() ).toBeFalsy()
        expect( found ).toBeFalsy()

        this.bot.parseInput( answer )
        found = this.bot.parseInput( question )

        expect( found ).toEqual( answer )
        expect( this.bot.isContextEmpty() ).toBeTruthy()
    })
})