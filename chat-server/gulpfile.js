const gulp = require('gulp')
const sass = require('gulp-sass')
const connect = require('gulp-connect')

gulp.task('sass', () => {
    gulp.src('./client/css/style.scss').
        pipe(sass({expended: true}).on('error', sass.logError)).
        pipe(gulp.dest('./client/css')).
        pipe(connect.reload())
})

gulp.task('default', () => {
    connect.server({livereload: true, root: 'client'})
    gulp.watch('./client/css/style.scss', ['sass'])
    gulp.watch('./client/index.html', function() {
        gulp.src('./client/index.html').pipe(connect.reload())
    })
})