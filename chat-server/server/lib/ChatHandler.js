const EVENTS = require('./events')

const ChatHandler = {
    clients: [],
    users: [],
    socket: null,

    init: function( socket ) {
        this.clients.push( socket )
        socket.on(EVENTS.JOIN, this.onJoin( socket, username ))
        socket.on(EVENTS.LEAVE, this.onLeave)
        socket.on(EVENTS.MSG, this.onMessage)
        socket.on(EVENTS.DISCONNECT, this.onDisconnect)
    },

    printStatus: function() {
        console.log('%s sockets connected', this.clients.length)
    },

    userLeft: function( username ) {
        console.log('%s has left the chat', username)
        this.users.splice(this.users.indexOf( username ), 1)
        this.socket.broadcast.emit(EVENTS.LEFT, { users: this.users, username })
    },

    onMessage: function( data ) {
        console.log('Incoming message:', data.username, data.msg)
        this.socket.broadcast.emit(EVENTS.MSG, data)
    },

    onLeave: function() {
        this.userLeft( this.socket.username )
    },

    onJoin: ( socket, username ) => {
        return () => {
            console.log('%s has joined the chat', username)
            socket.username = username
            this.users.push( socket.username )
            socket.broadcast.emit(EVENTS.JOINED, { users: this.users, username })
            this.printStatus()
        }
    },

    onDisconnect: function() {
        if ( !this.socket.username )
            return

        this.userLeft( this.socket.username )
        this.clients.splice(this.clients.indexOf( this.socket ), 1)
        console.log('Socket disconnected')
        this.printStatus()
    }
}

module.exports = ChatHandler