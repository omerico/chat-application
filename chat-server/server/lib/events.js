const EVENTS = {
    JOIN: 'chat:join',
    JOINED: 'chat:joined',
    LEAVE: 'chat:leave',
    LEFT: 'chat:left',
    MSG: 'chat:msg',
    DISCONNECT: 'disconnect'
}

module.exports = EVENTS