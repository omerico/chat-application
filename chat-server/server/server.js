const express = require('express')
const app = express()
const path = require('path')
const http = require('http').Server( app )
const io = require('socket.io')( http )
const port = process.env.PORT || 3000

app.use( express.static( path.join(__dirname, './../client') ) )
io.on('connection', require('./routes/socket'))
http.listen(port, () => console.log(`Server is now listening on port ${port}`))