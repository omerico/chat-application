const EVENTS = require('./../lib/events')

// Holds the whole sockets
const clients = []

// Holds only the usernames in the chat
const users = []

module.exports = function( socket ) {
    // Push the socket to the sockets list
    clients.push( socket )
    console.log('Connected: %s sockets connected', clients.length)

    socket.on(EVENTS.MSG, data => {
        console.log('Incoming message:', data)
        socket.broadcast.emit('chat:msg', data)
    })

    socket.on(EVENTS.JOIN, username => {
        console.log('User joined:', username)
        socket.username = username // Set the username as a property in the socket
        users.push( socket.username ) // Add the username to the usernames list
        socket.broadcast.emit(EVENTS.JOINED, { users, username }) // Notify the connections about the join
        socket.emit(EVENTS.JOINED, { users, username }) // Also reflect the event back to the specific socket because it need to receive the new list
    })

    socket.on(EVENTS.LEAVE, data => userLeft( socket.username ))

    socket.on(EVENTS.DISCONNECT, data => {
        if ( !socket.username )
            return

        // If user disconnects it counts like he left the chat, so we have to trigger the "left" event
        userLeft( socket.username )
        clients.splice(clients.indexOf( socket ), 1) // Remove it from the connections

        console.log('Socket Disconnected', clients.length)
        console.log('%s sockets connected', clients.length)
    })

    function userLeft( username ) {
        console.log('User left:', username)
        users.splice(users.indexOf( username ), 1) // Remove the user from the usernames list
        socket.broadcast.emit(EVENTS.LEFT, { users, username }) // Notify about the event
    }
}