const app = angular.module('ChatRoom', ['btford.socket-io'])

.constant('EVENTS', {
    JOIN: 'chat:join',
    JOINED: 'chat:joined',
    LEAVE: 'chat:leave',
    LEFT: 'chat:left',
    MSG: 'chat:msg',
    DISCONNECT: 'disconnect'
})

/**
 * "socketFactory" is provided by the "btford.socket-io" module
 * @see https://github.com/btford/angular-socket-io
 */
.factory('socket', function( socketFactory ) {
    return socketFactory()
})

/*
 .factory('ChatFactory', function( EVENTS, socket ) {
 let onlineUsers = []

 let items = {
 messages: [],
 notices: [],
 items: []
 }

 function getTime() {
 let now = new Date
 return now.getHours() + ':' + now.getMinutes()
 }

 function addItem( item, type = 'msg' ) {
 let newItem = item
 newItem.type = type

 items.items.push( newItem )

 if ( type === 'msg' )
 items.messages.push( item )
 else
 items.notices.push( item )
 }

 return {
 setOnlineUsers: function( users ) {
 return onlineUsers = users
 },

 sendMessage: function( message ) {

 }
 }
 })
*/

/**
 * Chat Controller
 */
.controller('ChatController', ['$scope', 'socket', 'focus', 'EVENTS', ( $scope, socket, focus, EVENTS ) => {
    $scope.onlineUsers = []
    $scope.items = {
        messages: [],
        notices: [],
        items: []
    }

    socket.on( EVENTS.JOINED, userJoined )
    socket.on( EVENTS.LEFT, userLeft )
    socket.on( EVENTS.MSG, incomingMessage )

    $scope.joinChat = function( e ) {
        e.preventDefault()
        $scope.username = $scope.temp_username
        $scope.message = `Hi there everyone! my name is ${$scope.username} :)`
        socket.emit( EVENTS.JOIN, $scope.username )
        focus('message')
    }

    $scope.sendMessage = function( e ) {
        e.preventDefault()

        if ( $scope.message.length < 3 )
            return false

        let data = { msg: $scope.message, username: $scope.username }
        socket.emit( EVENTS.MSG, data )
        addItem( data )
        $scope.message = ''
        focus('message')
    }

    function userLeft( data ) {
        $scope.onlineUsers = data.users
        addItem({ event: EVENTS.LEFT, username: data.username }, 'notice')
    }

    function userJoined( data ) {
        $scope.onlineUsers = data.users
        addItem({ event: EVENTS.JOIN, username: data.username }, 'notice')
    }

    function incomingMessage( data ) {
        addItem( data )
    }

    function addItem( item, type = 'msg' ) {
        let newItem = item
        newItem.type = type

        $scope.items.items.push( newItem )

        if ( type === 'msg' )
            $scope.items.messages.push( item )
        else
            $scope.items.notices.push( item )

        console.log($scope.items)
    }

    function getTime() {
        let now = new Date
        return now.getHours() + ':' + now.getMinutes()
    }
}])

.controller('UserController', ['$scope', 'focus', ( $scope, focus ) => {
    $scope.username = null
    $scope.temp_username = null

    if ( $scope.username === null )
        focus('username')
}])


.factory('focus', ['$timeout', '$window', ( $timeout, $window ) => {
    return id => {
        $timeout(() => {
            let element = $window.document.getElementById( id )
            if ( element )
                element.focus()
        })
    }
}])

.directive('scrollBottom', ['$timeout', ( $timeout ) => {
    return {
        restrict: 'A',
        link: function( scope, element, attr ) {
            scope.$watchCollection(attr.scrollBottom, () => $timeout(
                () => element[0].scrollTop = element[0].scrollHeight
            ))
        }
    }
}])

.filter('notice', EVENTS => {
    return item => {
        let announcement = `${item.username} has just `
        announcement += item.event === EVENTS.JOIN ? 'joined' : 'left'
        announcement += ' the chat'

        return announcement
    }
})