Chat Application With Bot

The application divided into two components:

1. The main chat server that handles all the connections and in charge of managing all the events
2. Bot micro service - acts as a normal client and implement "question answering" mechanism

Please run chat server with $ npm start and connect to port 3000
Afterwards, please launch the bot application with $ npm start
Then, connect one more client from the web making the chat have 3 connections

The bot will answer questions after he gets answers for them
